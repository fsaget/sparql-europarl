# SPARQL Europarl

A semantification of electoral data from the European parliament elections, as part of the 2021/2022 "Web semantics" project (Master 1 VICO @ Université de Nantes).

## Group members

[Thomas Georges](https://gitlab.com/ThomGeor) (Master 1 ALMA)

[Léo Olivier](https://gitlab.com/ekkaia) (Master 1 ALMA)

[Félix Saget](https://gitlab.com/fsaget) (Master 1 VICO)

## Data

We got our data from the [latest official European elections results datasheets](https://www.data.gouv.fr/fr/datasets/resultats-des-elections-europeennes-2019) provided by the French government. Among the available CSVs, we picked `resultats-definitifs-par-circonscription`.

### RDF triples

The CSV data was transformed into Turtle triplets using [TARQL](https://tarql.github.io). The construct query can be found in [`queries/construct.rq`](./queries/construct.rq). To rebuild the TTL, run :

```
./tarql-1.2/bin/tarql queries/construct.rq data/europarl2019.csv > data/europarl2019.ttl
```
You will have to prepend the content of [`data/ontology.ttl`](./data/ontology.ttl) to the generated file. This is because when including the ontology definitions to `queries/construct.rq`, they would get repeated for each line of the CSV in the generated file. This behavior is tied to TARQL's inner configuration.

### Query the data

In the [queries](./queries/) folder, you will find some sample queries that you can run against the dataset either directly with TARQL (see the [official TARQL documentation](https://tarql.github.io)) or through our SPARQL endpoint.

## SPARQL endpoint

We used [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2) to set up an endpoint for our SPARQL data.

### Launching the server locally

```
./apache-jena-fuseki-4.2.0/fuseki-server -v
```

### Importing & querying data using the GUI

Launch your Fuseki local server using the command above and navigate to `localhost:3030`. You should see the default Fuseki web interface. To import the TTL file into a dataset, switch to the *Manage datasets* tab and create a new dataset with the name of your choice. You can select the *in-memory* type.

Click on the *Upload data* button next to your newly created dataset and select the [TTL file](./data/europarl2019.ttl) from this project. When you see the file in the list, click *Upload now* and wait for the operation to finish. You don't need to specify a target graph name unless you plan on adding other data to the same dataset.

Once the data is imported, you can navigate back to the *Query* tab and paste in our [sample queries](./queries/).

### Importing & querying data using the API

## How it's going

- [x] Transform the CSV data into RDF triples
- [x] Demonstrate [sample queries](../queries)
- [x] Linking our data with another group's
- [x] Making inferences on a RDFS/OWL ontology
- [x] use VOID vocabulary to describe the dataset